# create two containers using Helm Chart

The aim of this project is to create two containers for backend and frontend application and using helm chart and enable the frontend to access the backend with ingress controller

## Steps

1. Create full stack application using Nodejs, react for frontend, and mongoDB for database
2. Create Dockerfile for both frontend and backend
3. Use Docker compose to create multi-container
4. Run the following commands

- Docker compose build

- Run the containers separately

```
docker compose up -d mongo
docker compose up -d api
docker-compose up -d client
```
- On the browser, run the client _localhost:8080_

- docker login

- tag the images

```
docker tag image_name ogahemmy/create_container_with_helm-client:latest

docker tag image_name ogahemmy/create_container_with_helm-api:latest

```
- Push image to docker hub

```
docker push ogahemmy/create_container_with_helm-client
docker push ogahemmy/create_container_with_helm-api
```

- Create deployment file called deploy.yaml file and add the deployment and service code

- Run Download and run helm chart to create the containers
1. Download
2. Search Helm - helm search hub node
3. Install Helm chart 
- helm repo add bitnami https://charts.bitnami.com/bitnami
- helm install api-container --set repository=,mongodb.enabled=true bitnami/node
4. Check if Helm chart is deploy - _kubectl get all_
5. Deploy Ingress Controller using Helm chart for ingress contrller
1. Add _helm chart_ repo _https://kubernetes-charts.storage.googleapis.com_
2. Install _ingress helm chart_ **helm install nginx-ingress stable/nginx-ingress --set controller.publishservice.enabled=true**
3. Run _kubectl get pod_ and check the ingress pod is created
- Cretate Ingress rule 
1. Confirm the ingress service is created _kubectl get svc_
2. Create ingress rule for client container so we can access it from browser
